//anagramGenerator.js
//Autor: DerexScript
'use strict';
process.stdout.write('\x1Bc');

var myArgs = process.argv.slice(2);
if(myArgs.length == 0){
	console.log("you must pass at least one parameter");
	process.exit(1); //EXIT FAILURE
}

let str = myArgs.join(' ');

let nRand;
let nRandArr = [];

let rep1 = "";
let rep2 = "";

String.prototype.replaceAt = function(index, replacement) {
	if (index >= this.length) {
		return this.valueOf();
	}
	return this.substring(0, index) + replacement + this.substring(index + 1);
}

for(let i = 0; i < str.length-1; i++){
	do{
		nRand = Math.floor(Math.random() * (str.length) + 0);
	}while(nRandArr.indexOf(nRand) > -1);
	nRandArr.push(nRand);

	//Debug
	console.log("posicao "+i+" subistituida pela "+nRand+" - "+str[i]+" subistituido por "+str[nRand]);

	rep1 = str[i];
	rep2 = str[nRand];
	str = str.replaceAt(i, rep2);
	str = str.replaceAt(nRand, rep1);
	rep1 = "";
	rep2 = "";
}

console.log("\n"+str+"\n");